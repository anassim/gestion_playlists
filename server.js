'use-strict'

const express = require('express')
const bodyParser = require('body-parser')

const apiPlaylist = require('./src/service/playlist-api')

const app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())

app.use(express.static('dist'))

// les infos du serveur
const PORT = 8080
const HTTP_OK = 200
const CONTENT_TYPE_JSON = 'application/json'
const CONTENT_TYPE_HTML = 'text/html'

// utilisation du CORS
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

// avoir les infos de toute les playlists
app.get('/api/playlists', function (request, response) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })

    apiPlaylist.getPlaylists((resultat) => {
        response.end(JSON.stringify(resultat.rows))
    })
})

// avoir les tracks d'une playlist
app.get('/api/playlist/:id', function (request, response) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })

    apiPlaylist.getTracksByIdPlaylist(request.params.id, (resultat) => {
        response.end(JSON.stringify(resultat.rows))
    })
})

// ajouter un track a une playlist
app.post('/api/playlist', function (request, response) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })

    apiPlaylist.addTrackToPlaylist(request.body.playlistId, request.body.title, request.body.uri, request.body.masterId)

    response.end()
})

// lancement du serveur ----------------------------------------------------------------------------
app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
})
