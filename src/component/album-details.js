import React from 'react'

// creer un li pour chaque track en remplissant les infos
const affichageAlbum = (track, index, onTrackAdded) => {
    return (
        <li className={index + ' list-group-item list-group-item-light list-group-item-action '} key={index} onClick={onTrackAdded}>
            {track.title}
        </li>
    )
}

// creer un div contenant tout les tracks d'un album
const AlbumDetails = ({ id, tracks, onTrackAdded }) => (
    <div id={id} className='d-flex justify-content-center align-items-center mt-3'>
        <div>
            <img src={tracks.images[0].uri} alt='image-album' className='w-75' />
        </div>
        <ul>
            {tracks.videos !== undefined ? tracks.videos.map((track, index) => affichageAlbum(track, index, onTrackAdded)) : ''}
        </ul>
    </div>
)

export default AlbumDetails
