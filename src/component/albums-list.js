/* eslint-disable react/jsx-no-target-blank */
import React from 'react'

// afficher la liste d'info pour une propriete de l'album
const listerInfos = function (infos) {
    return (
        infos.map((info, index) => <span key={index}>{info}, </span>)
    )
}

// creer un div pour chaque album en remplissant les infos
const affichageAlbum = (album, index, onAfficherTrack) => {
    const urlDiscogs = 'https://www.discogs.com' + album.uri
    const altImage = 'image-' + album.title

    return (
        <div className={album.master_id + ' album-div w-50 mt-5'} key={index} onClick={onAfficherTrack}>
            <figure className='w-25 float-left'>
                <img src={album.cover_image} className='w-75' alt={altImage} />
            </figure>
            <div id='details-album' className='w-50 float-left'>
                <b>title : </b> {album.title} <br />
                <b>country : </b> {album.country}<br />
                <b>format : </b> {listerInfos(album.format)}<br />
                <b>year : </b> {album.year} <br />
                <b>genre : </b> {listerInfos(album.genre)} <br />
                <a href={urlDiscogs} target='_blank'>mode details</a> <br />
            </div>
        </div>
    )
}

// creer les divs de tout les albums
const AlbumsList = ({ id, albums, onAfficherTrack }) => (
    <div id={id} className='d-flex flex-column align-items-center'>
        {albums.map((album, index) => affichageAlbum(album, index, onAfficherTrack))}
    </div>
)

export default AlbumsList
