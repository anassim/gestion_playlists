import React from 'react'

import SearchBar from 'component/search-bar'
import SelectForm from 'component/select-form'

const Navbar = ({ id, optionsPlayLists, onSelectionClicked, onSearchClicked }) => (
    <div id={id} className='navbar navbar-dark bg-dark'>
        <SelectForm
            text='Ecouter'
            id='liste-playlists'
            name='playlist'
            options={optionsPlayLists}
            onSelectionClicked={onSelectionClicked}
        />
        <SearchBar
            type='search'
            id='search'
            name='key'
            onSearchClicked={onSearchClicked}
        /><br />
    </div>
)

export default Navbar
