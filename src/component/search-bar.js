import React from 'react'

const SearchBar = ({ type, id, name, onSearchClicked }) => (
    <div className='form-inline'>
        <input className='form-control mr-sm-2' type={type} id={id} name={name} />
        <button className='btn btn-outline-success my-2 my-sm-0' id='search-button' onClick={onSearchClicked}>Search</button>
    </div>
)

export default SearchBar
