import React from 'react'
import YouTube from 'react-youtube'

// creer un li pour chaque track en remplissant les infos
const affichageAlbum = (track, index, onListenTrack) => {
    return (
        <li className={index + ' list-group-item list-group-item-light list-group-item-action'} key={index} onClick={onListenTrack}>
            {track.title}
        </li>
    )
}

const TrackList = ({ id, playlist, currentIndex, onListenTrack, onNextTrack, onPrecedentTrack }) => {
    if (playlist[0] === undefined) {
        return null
    }
    const optsVid = {
        height: '390',
        width: '640',
        playerVars: { autoplay: 1 }
    }
    const idVidFirstTrack = playlist[currentIndex].uri.replace('https://www.youtube.com/watch?v=', '')

    return (
        <div id={id} className='d-flex justify-content-center align-items-center mt-4'>
            <div className='d-flex flex-column align-items-center'>
                <YouTube className='ml-2' id='youtube-track' videoId={idVidFirstTrack} opts={optsVid} onEnd={onNextTrack} />
                <div className='d-flex'>
                    <button onClick={onPrecedentTrack}><b>&larr;</b></button>
                    <button onClick={onNextTrack}><b>&rarr;</b></button>
                </div>
            </div>
            <ul className='mr-2'>
                {playlist.map((track, index) => affichageAlbum(track, index, onListenTrack))}
            </ul>
        </div>
    )
}
export default TrackList
