import React, { Component } from 'react'

// les fonctions de presentation
import AlbumsList from '../component/albums-list'
import Navbar from '../component/navbar'
import AlbumDetails from '../component/album-details'
import TrackList from '../component/tracks-list'

// utilisation de l'api de discogs
const discogs = require('../service/discogs')

// eslint-disable-next-line no-unused-vars
class ApplicationContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            listePlaylists: [],
            playlistActuelle: { title: 'default' },
            listeTracks: [],
            currentTrackIndex: 0,
            albumsResultsList: [],
            tracksResult: [],
            masterIdActuel: 0,
            estAlbumsAffiches: false,
            estTracksAffiches: false,
            estPlaylistAffichees: false
        }

        this.handleAffichagePlayList = this.handleAffichagePlayList.bind(this)
        this.handleSearchTracks = this.handleSearchTracks.bind(this)
        this.handleAffichageTracks = this.handleAffichageTracks.bind(this)
        this.handleAjoutTrackToPlaylist = this.handleAjoutTrackToPlaylist.bind(this)
        this.handleListenTrack = this.handleListenTrack.bind(this)
        this.handleNextTrack = this.handleNextTrack.bind(this)
        this.handlePrecedentTrack = this.handlePrecedentTrack.bind(this)
    }

    // --------------------------------------------------------------------------------------------------------------------

    // afficher les tracks de l'album selectionne
    handleAffichagePlayList (event) {
        // cache les infos de recherche
        this.setState({
            estTracksAffiches: false,
            estAlbumsAffiches: false,
            currentTrackIndex: 0
        })

        // loader les track de la playlist selectionne
        const url = 'http://localhost:8080/api/playlist/' + (document.getElementById('liste-playlists').selectedIndex + 1)

        fetch(url, { method: 'GET' })
            .then(response => response.json())
            .then(response => {
                this.setState({
                    listeTracks: response,
                    playlistActuelle: { title: document.getElementById('liste-playlists').value },
                    estPlaylistAffichees: true
                })
            })
    }

    // --------------------------------------------------------------------------------------------------------------------

    handleListenTrack (event) {
        this.setState({ currentTrackIndex: parseInt(event.target.classList[0]) })
    }

    // --------------------------------------------------------------------------------------------------------------------

    handleNextTrack (event) {
        let index = this.state.currentTrackIndex + 1
        if (index === this.state.listeTracks.length) {
            index = 0
        }
        this.setState({ currentTrackIndex: index })
    }

    handlePrecedentTrack (event) {
        let index = this.state.currentTrackIndex - 1
        if (index === -1) {
            index = 0
        }
        this.setState({ currentTrackIndex: index })
    }

    // --------------------------------------------------------------------------------------------------------------------

    // faire une recherche d'albums sur discogs
    handleSearchTracks (event) {
        // cache les infos du track et playlist
        this.setState({
            estTracksAffiches: false,
            estPlaylistAffichees: false
        })
        const searchValue = document.getElementById('search').value // recuperer la cle de recherche
        // envoyer une requete vers le srv discogs avec la cle pour une recherche
        discogs.searchDiscogs(searchValue, { type: 'master', page: '1', per_page: '10' }, (error, data) => {
            if (error) {
                throw error
            } else {
                this.setState({
                    albumsResultsList: data.results,
                    estAlbumsAffiches: true
                })
            }
        })
    }

    // --------------------------------------------------------------------------------------------------------------------

    // afficher les infos detaillees d'un album
    handleAffichageTracks (event) {
        // cacher les resultat de recherche d'albums et playlist
        this.setState({
            estAlbumsAffiches: false,
            estPlaylistAffichees: false
        })
        const masterId = event.target.closest('.album-div').classList[0] // recuperer l'id master de l'album

        // envoyer une requete vers le srv discogs avec l'id pour voir les tracks
        discogs.searchMaster(masterId, (error, data) => {
            if (error) {
                throw error
            } else {
                this.setState({
                    masterIdActuel: masterId,
                    tracksResult: data,
                    estTracksAffiches: true
                })
            }
        })
    }

    // --------------------------------------------------------------------------------------------------------------------

    // ajout d'une chanson a la bd
    handleAjoutTrackToPlaylist (event) {
        const url = 'http://localhost:8080/api/playlist'
        const indexVideo = event.target.classList[0]
        event.target.classList.remove('list-group-item-light')
        event.target.classList.add('list-group-item-primary')
        const params = {
            playlistId: document.getElementById('liste-playlists').selectedIndex + 1,
            title: this.state.tracksResult.videos[indexVideo].title,
            uri: this.state.tracksResult.videos[indexVideo].uri,
            masterId: this.state.masterIdActuel
        }
        fetch(url, {
            method: 'POST',
            mode: 'no-cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
    }

    // --------------------------------------------------------------------------------------------------------------------

    getPlaylists () {
        // loader les playlists depuis l'api et la bd
        const url = 'http://localhost:8080/api/playlists'
        fetch(url, { method: 'GET' })
            .then(response => response.json())
            .then(response => {
                this.setState({
                    listePlaylists: response
                })
                this.setState({ playlistActuelle: this.state.listePlaylists[0] })
            })
    }

    // --------------------------------------------------------------------------------------------------------------------

    componentDidMount () {
        // si c la premiere qu'on rentre a l'apli en initialise la liste des playliste depuis la bd
        if (this.state.listePlaylists.length === 0) {
            this.getPlaylists()
        }
    }

    // --------------------------------------------------------------------------------------------------------------------

    render () {
        return (
            <div>
                <Navbar
                    id='navbar-search'
                    optionsPlayLists={this.state.listePlaylists}
                    onSelectionClicked={this.handleAffichagePlayList}
                    onSearchClicked={this.handleSearchTracks}
                />
                {this.state.estAlbumsAffiches ? <AlbumsList id='albums-list' albums={this.state.albumsResultsList} onAfficherTrack={this.handleAffichageTracks} /> : ''}
                {this.state.estTracksAffiches ? <AlbumDetails id='albums-details' tracks={this.state.tracksResult} onTrackAdded={this.handleAjoutTrackToPlaylist} /> : ''}
                {this.state.estPlaylistAffichees ? <TrackList id='playlist-tracks' playlist={this.state.listeTracks} currentIndex={this.state.currentTrackIndex} onListenTrack={this.handleListenTrack} onNextTrack={this.handleNextTrack} onPrecedentTrack={this.handlePrecedentTrack} /> : ''}
            </div>
        )
    }
}

export default ApplicationContainer
