const { Client } = require('pg')

let client = {}

// function pour se connecter a la bd
function connectBd () {
    client = new Client({
        host: 'localhost',
        port: 5432,
        database: 'tp_music',
        user: 'postgres',
        password: 'postgres'
    })

    client.connect((error) => {
        if (error) {
            throw error
        }
    })
}

// function pour la requete
function query (query, values, resultCallback) {
    client.query(query, values, (error, result) => {
        if (error) {
            throw error
        }
        resultCallback(result)
    })
}

// function pour se deconnecter de la bd
function disconnectBd () {
    client.end()
}

module.exports = {
    connect: connectBd,
    disconnect: disconnectBd,
    query: query
}
