const db = require('../server/bd')

// pour avoir toute les playlists
function getPlaylists (callback) {
    db.connect()
    db.query('SELECT * FROM playlist', null, (resultat) => {
        callback(resultat)
        db.disconnect()
    })
}

// pour avoir les tracks d'une playlist
function getTracksByIdPlaylist (idPlaylist, callback) {
    db.connect()
    db.query('SELECT * FROM track WHERE playlist_id = $1', [idPlaylist], (resultat) => {
        callback(resultat)
        db.disconnect()
    })
}

// pour ajouter un track a une playlist
function addTrackToPlaylist (playlistId, title, uri, masterId) {
    db.connect()
    db.query('INSERT INTO track (playlist_id, title, uri, master_id) VALUES ($1, $2, $3, $4)', [playlistId, title, uri, masterId], (resultat) => {
        db.disconnect()
    })
}

module.exports = {
    getPlaylists: getPlaylists,
    addTrackToPlaylist: addTrackToPlaylist,
    getTracksByIdPlaylist: getTracksByIdPlaylist
}
